import Vue from "vue";
import VueRouter from "vue-router";
import Prodotti from "../views/Prodotti";

Vue.use(VueRouter);

const routes = [
  {
    path: "/prodotti/:id/:titolo",
    name: "prodotti",
    component: Prodotti
  },
  {
    path: "/prodotto/:id/:titolo",
    name: "prodotto",
    component: () =>
      import(/* webpackChunkName: "prodotto" */ "../views/Prodotto.vue")
  },
  {
    path: "/carrello",
    name: "carrello",
    component: () =>
      import(/* webpackChunkName: "carrello" */ "../views/Carrello.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
